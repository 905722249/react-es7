/*
 * @Author: your name
 * @Date: 2021-08-16 17:51:36
 * @LastEditTime: 2021-08-17 11:44:17
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \hello-react\src\js\components\Hello.js
 */
import React, {Component, PropTypes} from 'react';
import {Button, Modal} from 'antd';
import 'antd/dist/antd.css';
import {checkLogin, BackGroundDecorator, ModelDecorator, log} from './untils';

// @ModelDecorator() //修饰弹框
// @BackGroundDecorator({background: '#ec7259'})//修饰背景
@log('1')
@log('2')
class ParentBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            userInfo: {id: 999}
        };
        this.handleAddTick = this.handleAddTick.bind(this);
    }

    @checkLogin(true) //修饰登录-状态成功
    @checkLogin(false) //修饰登录-状态成功
    handleAddTick() {
        this.setState({count: this.state.count + 1});
    };

    @checkLogin(false) //修饰登录-状态失败
    handleSubtract = () => {
        this.setState({count: this.state.count - 1});
    };

    render() {
        console.log('最里面', this);
        return (
            <div>
                <p>点击 {this.state.count} 计数</p>
                <Button className="btn-test"
                        onClick={this.handleAddTick.bind(this)}>点击增加</Button>
                <p className="btn-test"
                   onClick={this.handleSubtract}>点击减少</p>
            </div>
        );
    }
}

export default ParentBox;