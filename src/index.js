/*
 * @Author: your name
 * @Date: 2021-08-17 10:50:04
 * @LastEditTime: 2021-08-17 11:27:48
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \react-es7\src\index.js
 */

import React from 'react';
import ReactDOM from 'react-dom';
import ParentBox from './ParentBox';

const title = 'ES7-修饰器';

ReactDOM.render(
  <div>
      {title}
      <ParentBox title="你好"/>
      </div>,
  document.getElementById('app')
);

module.hot.accept(); // 热更新