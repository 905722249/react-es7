import React, {Component, PropTypes} from 'react';
import {Modal, Button} from 'antd';

/**
 * log
 */

export function log(logger) {
    return function(target) {
        target.prototype.logger = logger;
        console.log('修饰log', logger);
    }
}

export const ModelDecorator = param => WrappedComponent => {
    console.log('修饰弹框');
    return class ModelBoxContent extends Component {
        constructor(props) {
            super(props);
            this.state = {
                show: true
            };
        }

        showModal = () => {
            console.log('完成', this.modelBoxContent);
        };

        hiddenModal = () => {
            console.log('取消');
            this.setState({show: false});
        };

        setModel = (show) => {
            this.setState({show: !!show});
        };

        render() {
            const {show} = this.state;

            return (
                <div>
                    <Modal
                        title="修饰器弹框"
                        visible={show}
                        onOk={this.showModal}
                        onCancel={this.hiddenModal}
                    >
                        <WrappedComponent
                            ref={ref => this.modelBoxContent = ref}
                            onOk={() => this.setModel(false)}
                            onCancel={() => this.setModel(false)}
                            {...this.props} >
                        </WrappedComponent>
                    </Modal>
                </div>
            )
        }
    }
};

export const checkLogin = (login) => (target, name, descriptor) => {
    let method = descriptor.value;

    // 模拟判断条件
    let isLogin = login || false;
    target.a = '登录属性-a'; //增加静态属性
    // target.prototype.b = '登录属性-b'; //增加可继承属性
    console.log('修饰登录', target, name, isLogin);
    descriptor.value = function(...args) {
        if (isLogin) {
            console.log('已经登录');
            console.log('target', target);
            console.log('name', name);
            console.log('descriptor', descriptor);
            method.apply(this, args);
        } else {
            console.log('没有登录，即将跳转到登录页面...');
        }
    };
    // console.log('函数', descriptor.value);
};

export const BackGroundDecorator = param => WrappedComponent => {
    console.log('修饰背景');
    // WrappedComponent.a = '背景属性-a'; //增加静态属性
    // WrappedComponent.prototype.b = '背景属性-b'; //增加可继承属性
    return class BackGroundContent extends Component {
        render() {
            let bgColor = param && param.background ? param.background : 'rgb(244,245,250)';
            return (
                <div style={{backgroundColor: bgColor}}>
                    <WrappedComponent
                        ref={ref => this.backGroundContent = ref}  {...this.props} />
                </div>
            )
        }
    }
};